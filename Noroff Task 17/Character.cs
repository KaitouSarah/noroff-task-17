﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Noroff_Task_17
{
    public class Character
    {
        private int hp;
        private string name;
        private int armorRating;
        private int energyOrMana;
        private Race race;

        public Character(int h, string n, int ar, int e, int r)
        {
            Hp = h;
            Name = n;
            ArmorRating = ar;
            EnergyOrMana = e;
            Race = (Race)r;
        }

        public string Attack()
        {
            string attackLine = $"\"Leeeerooooy Jeeeenkiiins\" {Name} shouted as he attacked the dragon.";
            Console.WriteLine(attackLine);
            Hp = 0;

            return attackLine;
        }

        public string Move()
        {
            string moveLine = $"{name} tried desperatly to move, but was frozen still as he watched " +
                $"his village burn to the ground.";
            Console.WriteLine(moveLine);

            return moveLine;
        }


        public int Hp { get => hp; set => hp = value; }
        public string Name { get => name; set => name = value; }
        public int ArmorRating { get => armorRating; set => armorRating = value; }
        public int EnergyOrMana { get => energyOrMana; set => energyOrMana = value; }
        public Race Race { get => race; set => race = value; }
    }
}
