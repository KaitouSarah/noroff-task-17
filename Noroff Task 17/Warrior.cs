﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Noroff_Task_17
{
    public class Warrior : Character
    {
        private string quote;
        private string rpgClass;

        public Warrior(int hp, string name, int armorRating, int energy, int race) : base(hp, name, armorRating, energy, race)
        {
            quote = "Spells? I cast axe!";

        }

        //Print all info about the character
        public void WriteCharacterInfo()
        {
            Console.WriteLine("/*CHARACTER SHEET/*");
            Console.WriteLine($"Name: {this.Name}");
            Console.WriteLine($"Hp: {this.Hp}");
            Console.WriteLine($"Energy: {this.EnergyOrMana}");
            Console.WriteLine($"Armor rating: {this.ArmorRating}");
            Console.WriteLine($"Race: {this.Race}");
            Console.WriteLine($"Quote: \"{quote}\"\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        }
        public string GetSummaryString()
        {
            return $"Name: {this.Name}\nRace: {this.Race}\nHp: {this.Hp}\nMana: {this.EnergyOrMana}\n" +
                $"Armor Rating: {this.ArmorRating}\nQuote: {this.Quote}";
        }

        public string Quote { get => quote; set => quote = value; }
        public string RpgClass { get => rpgClass; set => rpgClass = value; }
    }
}
