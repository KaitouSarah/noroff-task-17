﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Noroff_Task_17
{
    public class Wizard : Character
    {
        private string quote;
        private string rpgClass;

        public Wizard(int hp, string name, int armorRating, int mana, int race) : base(hp, name, armorRating, mana, race)
        {
            Quote = "Your deeds cannot be undone. You, however, can be. *casts Disintegrate*";
        }


        //Print all info about the character
        public void WriteCharacterInfo()
        {
            Console.WriteLine("/*CHARACTER SHEET*/");
            Console.WriteLine($"Name: {this.Name}");
            Console.WriteLine($"Hp: {this.Hp}");
            Console.WriteLine($"Mana: {this.EnergyOrMana}");
            Console.WriteLine($"Armor rating: {this.ArmorRating}");
            Console.WriteLine($"Race: {this.Race}");
            Console.WriteLine($"Quote: \"{Quote}\"\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        }

        public string GetSummaryString()
        {
            return $"Name: {this.Name}\nRace: {this.Race}\nHp: {this.Hp}\nMana: {this.EnergyOrMana}\n" +
                $"Armor Rating: {this.ArmorRating}\nQuote: {this.Quote}";
        }

        public string Quote { get => quote; set => quote = value; }
        public string RpgClass { get => rpgClass; set => rpgClass = value; }
    }
}
