﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Noroff_Task_17
{
    public enum Race
    {
        Elf,
        Dwarf,
        Human,
        Hobgoblin,
        Lizardfolk
    }
}
